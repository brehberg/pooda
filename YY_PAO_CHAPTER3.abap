*&---------------------------------------------------------------------*
*&  Practical Object-Oriented Design in ABAP
*&    based on POODR:  Chapter 3;  Page 36
*&---------------------------------------------------------------------*
REPORT yy_pao_chapter3_page36.

CLASS lcl_gear DEFINITION.
  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING iv_chainring TYPE i
                  iv_cog       TYPE i
                  iv_rim       TYPE i
                  iv_tire      TYPE f,
      gear_inches
        RETURNING VALUE(rv_gear_inches) TYPE f,
      ratio
        RETURNING VALUE(rv_ratio) TYPE f,
      chainring
        RETURNING VALUE(rv_chainring) TYPE i,
      cog
        RETURNING VALUE(rv_cog) TYPE i,
      rim
        RETURNING VALUE(rv_rim) TYPE i,
      tire
        RETURNING VALUE(rv_tire) TYPE f.
    "...

  PRIVATE SECTION.
    DATA:
      mv_chainring TYPE i,
      mv_cog       TYPE i,
      mv_rim       TYPE i,
      mv_tire      TYPE f.
ENDCLASS.

CLASS lcl_wheel DEFINITION.
  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING iv_rim  TYPE i
                  iv_tire TYPE f,
      diameter
        RETURNING VALUE(rv_diameter) TYPE f,
      rim
        RETURNING VALUE(rv_rim) TYPE i,
      tire
        RETURNING VALUE(rv_tire) TYPE f.
    "...

  PRIVATE SECTION.
    DATA:
      mv_rim  TYPE i,
      mv_tire TYPE f.
ENDCLASS.

CLASS lcl_gear IMPLEMENTATION.
  METHOD constructor.
    mv_chainring = iv_chainring.
    mv_cog       = iv_cog.
    mv_rim       = iv_rim.
    mv_tire      = iv_tire.
  ENDMETHOD.

  METHOD gear_inches.
    rv_gear_inches = ratio( ) * NEW lcl_wheel( iv_rim  = rim( )
                                               iv_tire = tire( ) )->diameter( ).
  ENDMETHOD.

  METHOD ratio.
    rv_ratio = chainring( ) / CONV f( cog( ) ).
  ENDMETHOD.

  METHOD chainring.
    rv_chainring = mv_chainring.
  ENDMETHOD.

  METHOD cog.
    rv_cog = mv_cog.
  ENDMETHOD.

  METHOD rim.
    rv_rim = mv_rim.
  ENDMETHOD.

  METHOD tire.
    rv_tire = mv_tire.
  ENDMETHOD.
ENDCLASS.

CLASS lcl_wheel IMPLEMENTATION.
  METHOD constructor.
    mv_rim  = iv_rim.
    mv_tire = iv_tire.
  ENDMETHOD.

  METHOD diameter.
    rv_diameter = rim( ) + ( tire( ) * 2 ).
  ENDMETHOD.

  METHOD rim.
    rv_rim = mv_rim.
  ENDMETHOD.

  METHOD tire.
    rv_tire = mv_tire.
  ENDMETHOD.
ENDCLASS.


START-OF-SELECTION.

  DATA(go_gear) = NEW lcl_gear( iv_chainring = 52
                                iv_cog       = 11
                                iv_rim       = 26
                                iv_tire      = '1.5' ).
  cl_demo_output=>display( go_gear->gear_inches( ) ).
  " -> 137.09090909090909

*&---------------------------------------------------------------------*
*&    based on POODR:  Chapter 3;  Page 39
*&---------------------------------------------------------------------*
REPORT yy_pao_chapter3_page39.

CLASS lcl_gear DEFINITION.
  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING iv_chainring TYPE i
                  iv_cog       TYPE i
                  iv_rim       TYPE i
                  iv_tire      TYPE f,
      gear_inches
        RETURNING VALUE(rv_gear_inches) TYPE f.
    "...

  PRIVATE SECTION.
    DATA:
      mv_chainring TYPE i,
      mv_cog       TYPE i,
      mv_rim       TYPE i,
      mv_tire      TYPE f.
ENDCLASS.

CLASS lcl_gear IMPLEMENTATION.
  METHOD constructor.
    mv_chainring = iv_chainring.
    mv_cog       = iv_cog.
    mv_rim       = iv_rim.
    mv_tire      = iv_tire.
  ENDMETHOD.

  METHOD gear_inches.
    rv_gear_inches = ratio( ) * NEW lcl_wheel( iv_rim  = rim( )
                                               iv_tire = tire( ) )->diameter( ).
  ENDMETHOD.

  "...
ENDCLASS.


START-OF-SELECTION.

  DATA(go_gear) = NEW lcl_gear( iv_chainring = 52
                                iv_cog       = 11
                                iv_rim       = 26
                                iv_tire      = '1.5' ).

*&---------------------------------------------------------------------*
*&    based on POODR:  Chapter 3;  Page 41
*&---------------------------------------------------------------------*
REPORT yy_pao_chapter3_page41.

CLASS lcl_gear DEFINITION.
  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING iv_chainring TYPE i
                  iv_cog       TYPE i
                  io_wheel     TYPE REF TO object,
      gear_inches
        RETURNING VALUE(rv_gear_inches) TYPE f,
      diameter
        RETURNING VALUE(rv_diameter) TYPE f.
    "...

  PRIVATE SECTION.
    DATA:
      mv_chainring TYPE i,
      mv_cog       TYPE i,
      mo_wheel     TYPE REF TO object.
ENDCLASS.

CLASS lcl_gear IMPLEMENTATION.
  METHOD constructor.
    mv_chainring = iv_chainring.
    mv_cog       = iv_cog.
    mo_wheel     = io_wheel.
  ENDMETHOD.

  METHOD gear_inches.
    rv_gear_inches = ratio( ) * diameter( ).
  ENDMETHOD.

  METHOD diameter.
    DATA(lo_wheel) = wheel( ).
    DATA(lv_method) = |DIAMETER|.
    CALL METHOD lo_wheel->(lv_method)
      RECEIVING rv_diameter = rv_diameter.
  ENDMETHOD.

  "...
ENDCLASS.


START-OF-SELECTION.

  " Gear expects a 'Duck' that knows 'diameter'
  DATA(go_gear) = NEW lcl_gear( iv_chainring = 52
                                iv_cog       = 11
                                io_wheel     = NEW lcl_wheel( iv_rim  = 26
                                                              iv_tire = '1.5' ) ).

*&---------------------------------------------------------------------*
*&    based on POODR:  Chapter 3;  Page 43
*&---------------------------------------------------------------------*
REPORT yy_pao_chapter3_page43a.

CLASS lcl_gear DEFINITION.
  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING iv_chainring TYPE i
                  iv_cog       TYPE i
                  iv_rim       TYPE i
                  iv_tire      TYPE f,
      gear_inches
        RETURNING VALUE(rv_gear_inches) TYPE f.
    "...

  PRIVATE SECTION.
    DATA:
      mv_chainring TYPE i,
      mv_cog       TYPE i,
      mv_rim       TYPE i,
      mv_tire      TYPE f,
      mo_wheel     TYPE REF TO lcl_wheel.
ENDCLASS.

CLASS lcl_gear IMPLEMENTATION.
  METHOD constructor.
    mv_chainring = iv_chainring.
    mv_cog       = iv_cog.
    mv_rim       = iv_rim.
    mv_tire      = iv_tire.
    mo_wheel     = NEW #( iv_rim  = iv_rim
                          iv_tire = iv_tire ).
  ENDMETHOD.

  METHOD gear_inches.
    rv_gear_inches = ratio( ) * wheel( )->diameter( ).
  ENDMETHOD.

  "...
ENDCLASS.

*&---------------------------------------------------------------------*
*&    based on POODR:  Chapter 3;  Page 43
*&---------------------------------------------------------------------*
REPORT yy_pao_chapter3_page43b.

CLASS lcl_gear DEFINITION.
  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING iv_chainring TYPE i
                  iv_cog       TYPE i
                  iv_rim       TYPE i
                  iv_tire      TYPE f,
      gear_inches
        RETURNING VALUE(rv_gear_inches) TYPE f.
    "...

  PRIVATE SECTION.
    DATA:
      mv_chainring TYPE i,
      mv_cog       TYPE i,
      mv_rim       TYPE i,
      mv_tire      TYPE f,
      mo_wheel     TYPE REF TO lcl_wheel.
ENDCLASS.

CLASS lcl_gear IMPLEMENTATION.
  METHOD constructor.
    mv_chainring = iv_chainring.
    mv_cog       = iv_cog.
    mv_rim       = iv_rim.
    mv_tire      = iv_tire.
  ENDMETHOD.

  METHOD gear_inches.
    rv_gear_inches = ratio( ) * wheel( )->diameter( ).
  ENDMETHOD.

  METHOD wheel.
    IF mo_wheel IS NOT BOUND.
      mo_wheel = NEW #( iv_rim  = rim( )
                        iv_tire = tire( ) ).
    ENDIF.
    ro_wheel = mo_wheel.
  ENDMETHOD.

  "...
ENDCLASS.

*&---------------------------------------------------------------------*
*&    based on POODR:  Chapter 3;  Page 44
*&---------------------------------------------------------------------*
REPORT yy_pao_chapter3_page44.


  METHOD gear_inches.
    rv_gear_inches = ratio( ) * wheel( )->diameter( ).
  ENDMETHOD.


  METHOD gear_inches.
    "... a few lines of scary math
    DATA(lv_foo) = some_intermediate_result( ) * wheel( )->diameter( ).
    "... more lines of scary math
  ENDMETHOD.

*&---------------------------------------------------------------------*
*&    based on POODR:  Chapter 3;  Page 45
*&---------------------------------------------------------------------*
REPORT yy_pao_chapter3_page45.


  METHOD gear_inches.
    "... a few lines of scary math
    DATA(lv_foo) = some_intermediate_result( ) * diameter( ).
    "... more lines of scary math
  ENDMETHOD.


  METHOD diameter.
    rv_diameter = wheel( )->diameter( ).
  ENDMETHOD.

*&---------------------------------------------------------------------*
*&    based on POODR:  Chapter 3;  Page 46
*&---------------------------------------------------------------------*
REPORT yy_pao_chapter3_page46.

CLASS lcl_gear DEFINITION.
  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING iv_chainring TYPE i
                  iv_cog       TYPE i
                  io_wheel     TYPE REF TO lcl_wheel.
    "...

  PRIVATE SECTION.
    DATA:
      mv_chainring TYPE i,
      mv_cog       TYPE i,
      mo_wheel     TYPE REF TO lcl_wheel.
ENDCLASS.

CLASS lcl_gear IMPLEMENTATION.
  METHOD constructor.
    mv_chainring = iv_chainring.
    mv_cog       = iv_cog.
    mo_wheel     = io_wheel.
  ENDMETHOD.

  "...
ENDCLASS.

  DATA(go_gear) = NEW lcl_gear( iv_chainring = 52
                                iv_cog       = 11
                                io_wheel     = NEW #( iv_rim  = 26
                                                      iv_tire = '1.5' ) ).

*&---------------------------------------------------------------------*
*&    based on POODR:  Chapter 3;  Page 47
*&---------------------------------------------------------------------*
REPORT yy_pao_chapter3_page47.

CLASS lcl_gear DEFINITION.
  PUBLIC SECTION.
    TYPES:
      to_wheel TYPE REF TO lcl_wheel.
    METHODS:
      constructor
        IMPORTING it_args TYPE abap_parmbind_tab.
    "...

  PRIVATE SECTION.
    DATA:
      mv_chainring TYPE i,
      mv_cog       TYPE i,
      mo_wheel     TYPE REF TO lcl_wheel.
ENDCLASS.

CLASS lcl_gear IMPLEMENTATION.
  METHOD constructor.
    mv_chainring = CAST i( it_args[ name = |chainring| ]-value )->*.
    mv_cog       = CAST i( it_args[ name = |cog| ]-value )->*.
    mo_wheel     = CAST to_wheel( it_args[ name = |wheel| ]-value )->*.
  ENDMETHOD.

  "...
ENDCLASS.


START-OF-SELECTION.

  DATA(go_wheel) = NEW lcl_gear=>to_wheel( iv_rim  = 26
                                           iv_tire = '1.5' ).

  DATA(go_gear) = NEW lcl_gear( VALUE #( ( VALUE #( name  = |chainring|
                                                    value = REF #( 52 ) ) )
                                         ( VALUE #( name  = |cog|
                                                    value = REF #( 11 ) ) )
                                         ( VALUE #( name  = |wheel|
                                                    value = REF #( go_wheel ) ) ) ) ).

*&---------------------------------------------------------------------*
*&    based on POODR:  Chapter 3;  Page 48
*&---------------------------------------------------------------------*
REPORT yy_pao_chapter3_page48.

  " specifying defaults using COND( ) & line_exists( )
  METHOD constructor.
    mv_chainring = COND #( WHEN line_exists( it_args[ name = |chainring| ] )
                           THEN CAST i( it_args[ name = |chainring| ]-value )->*
                           ELSE 40 ).
    mv_cog       = COND #( WHEN line_exists( it_args[ name = |cog| ] )
                           THEN CAST i( it_args[ name = |cog| ]-value )->*
                           ELSE 18 ).
    mo_wheel     = CAST to_wheel( it_args[ name = |wheel| ]-value )->*.
  ENDMETHOD.

*&---------------------------------------------------------------------*
*&    based on POODR:  Chapter 3;  Page 49
*&---------------------------------------------------------------------*
REPORT yy_pao_chapter3_page49a.

  " specifying defaults using VALUE( ) & DEFAULT
  METHOD constructor.
    mv_chainring = CAST i( LET duck = VALUE #( it_args[ name = |chainring| ]-value
                                               DEFAULT REF #( 40 ) )
                           IN duck )->*.
    mv_cog       = CAST i( LET duck = VALUE #( it_args[ name = |cog| ]-value
                                               DEFAULT REF #( 18 ) )
                           IN duck )->*.
    mo_wheel     = CAST to_wheel( it_args[ name = |wheel| ]-value )->*.
  ENDMETHOD.

*&---------------------------------------------------------------------*
*&    based on POODR:  Chapter 3;  Page 49
*&---------------------------------------------------------------------*
REPORT yy_pao_chapter3_page49b.

  " specifying defaults by filtering a defaults hashed table
  METHOD constructor.
    DATA(lt_args) = VALUE #( BASE it_args FOR default_arg IN
                               FILTER abap_parmbind_tab( default_args( )
                                 EXCEPT IN it_args WHERE name = name )
                             ( default_arg ) ).
    mv_chainring = CAST i( lt_args[ name = |chainring| ]-value )->*.
    mv_cog       = CAST i( lt_args[ name = |cog| ]-value )->*.
    mo_wheel     = CAST to_wheel( lt_args[ name = |wheel| ]-value )->*.
  ENDMETHOD.

  METHOD defaults.
    rt_args = VALUE #( ( VALUE #( name  = |chainring|
                                  value = REF i( 40 ) ) )
                       ( VALUE #( name  = |cog|
                                  value = REF i( 18 ) ) ) ).
  ENDMETHOD.

*&---------------------------------------------------------------------*
*&    based on POODR:  Chapter 3;  Page 52
*&---------------------------------------------------------------------*
REPORT yy_pao_chapter3_page52.

CLASS lcl_gear DEFINITION.
  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING iv_chainring TYPE i
                  iv_cog       TYPE i,
      gear_inches
        IMPORTING iv_diameter           TYPE f
        RETURNING VALUE(rv_gear_inches) TYPE f,
      ratio
        RETURNING VALUE(rv_ratio) TYPE f.
    "...

  PRIVATE SECTION.
    DATA:
      mv_chainring TYPE i,
      mv_cog       TYPE i.
ENDCLASS.

CLASS lcl_wheel DEFINITION.
  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING iv_rim       TYPE i
                  iv_tire      TYPE f
                  iv_chainring TYPE i
                  iv_cog       TYPE i,
      diameter
        RETURNING VALUE(rv_diameter) TYPE f,
      gear_inches
        RETURNING VALUE(rv_gear_inches) TYPE f.
    "...

  PRIVATE SECTION.
    DATA:
      mv_rim  TYPE i,
      mv_tire TYPE f,
      mo_gear TYPE REF TO lcl_gear.
ENDCLASS.

CLASS lcl_gear IMPLEMENTATION.
  METHOD constructor.
    mv_chainring = iv_chainring.
    mv_cog       = iv_cog.
  ENDMETHOD.

  METHOD gear_inches.
    rv_gear_inches = ratio( ) * iv_diameter.
  ENDMETHOD.

  METHOD ratio.
    rv_ratio = chainring( ) / CONV f( cog( ) ).
  ENDMETHOD.

  "...
ENDCLASS.

CLASS lcl_wheel IMPLEMENTATION.
  METHOD constructor.
    mv_rim  = iv_rim.
    mv_tire = iv_tire.
    mo_gear = NEW #( iv_chainring = iv_chainring
                     iv_cog       = iv_cog ).
  ENDMETHOD.

  METHOD diameter.
    rv_diameter = rim( ) + ( tire( ) * 2 ).
  ENDMETHOD.

  METHOD gear_inches.
    rv_gear_inches = gear( )->gear_inches( diameter( ) ).
  ENDMETHOD.

  "...
ENDCLASS.


START-OF-SELECTION.

  DATA(go_wheel) = NEW lcl_wheel( iv_rim       = 26
                                  iv_tire      = '1.5'
                                  iv_chainring = 52
                                  iv_cog       = 11 ).
